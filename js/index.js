// Need to be implement
let currentStatus = "ALL";

function generateItem (item) {
    return '<li class="' + item.status +'"><input type="checkbox" ' 
    + (item.status === 'COMPLETED' ? 'checked' : '') 
    + ' onclick="changeStatus(' + item.id + ', \'' + item.text + '\', \'' + item.status + '\')"/><span id="' 
    + item.id + '" onclick="makeEditable(this)" onkeydown="editItem(this, ' + '\'' 
    + item.status + '\'' + ')" onmouseout="makeDiseditable(this)">' 
    + item.text + '</span><button class="deleteButton" onclick="deleteItems(' + item.id +')"></button></li>'
}

function showItems (status, items) {
    currentStatus = status;
    document.getElementById("myList").innerHTML = generateItems(items);
}

function filterItems (items) {
    if (currentStatus === "ALL" && items !== undefined) {
        return items;
    } else if (currentStatus !== "ALL" && items !== undefined) {
        return items.filter(item => item.status === currentStatus);
    }
}

function generateItems (items) {
    return filterItems(items).map(item => generateItem(item)).reduce((a, b) => a + b, "");
}


function addToDoItem () {
    let text = document.getElementById("itemToAdd").value;
    let item = {
        text: text,
    };
    addItems(item);
    document.getElementById("itemToAdd").value = "";
}

function enterKey (key) {
    if (key.keyCode === 13) {
        addToDoItem();
    }
}

function changeStatus (id, text, status) {
    let updatedItem = 
        {
            id: id,
            text: text,
            status: (status === "COMPLETED" ? "ACTIVE" : "COMPLETED")
        };
    updateItems(updatedItem);
}

function checkFootButton (dom, status) {
    let elements = dom.parentNode.children;
    Array.from(elements).forEach((element) => element.classList.remove('footButtonChecked'));
    dom.classList.add('footButtonChecked');
    getAllItems(status);
}

function makeEditable(element) {
    element.setAttribute("contenteditable", true);
}

function makeDiseditable(element) {
    element.setAttribute("contenteditable", false);
}

function editItem(element, status) {
    currentStatus = status;
    if (event.keyCode == 13) {
        const id = element.getAttribute("id");
        const text = element.innerText;
        let updatedItem = 
            {
                id: id,
                text: text,
                status: currentStatus
            };
        updateItems(updatedItem);
    }
}


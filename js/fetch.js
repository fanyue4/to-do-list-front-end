const url = 'http://localhost:8080/items';
function getAllItems (currentStatus) {
    fetch(url)
      .then(response => {
          return response.json();
        })
      .then(items => {
          showItems(currentStatus, items);
        })
}

function addItems(item) {
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(item)
    })
    .then((response) => {
        return response.json();
      })
    .then(() => getAllItems(currentStatus));
}

function deleteItems(id) {
    fetch(url + '/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then(() => getAllItems(currentStatus));
}

function updateItems(item) {
    fetch(url, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
    .then(response => {
        return response.json();
      })
    .then(() => getAllItems(currentStatus));
}
    